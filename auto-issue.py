#!/usr/bin/python3
from vars import *
import argparse as ap
import json
import requests

def send_request(project_id,issue):
    url = F'''{base_url}/{project}/issues?title={issue}'''
#    print(url)
    r = requests.post(url,headers=headers)
    print(r.status_code,r.reason,issue)


#create arg parse for time of week
parser = ap.ArgumentParser()
parser.add_argument('--weekly',help='run issues in the monthly list',action='store_true')
parser.add_argument('--monthly',help='run issues in the weekly list',action='store_true')
parser.add_argument('--daily',help='run issues in the daily list',action='store_true')
args = parser.parse_args()

if args.weekly:
    time_period = 'weekly_issues'
elif args.monthly:
    time_period = 'monthly_issues'
elif args.daily:
    time_period = 'daily_issues'
else:
    print('please rerun the program with a valid time frame to see all options rerun the script with --help')
    exit()

for project, issues in projects.items():
    for issue in issues[time_period]:
        send_request(project_id=project,issue=issue)


